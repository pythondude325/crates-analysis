use anyhow::Context;
use lasso::Spur;
use serde::{Deserialize, Serialize};
use std::borrow::Cow;
use std::collections::HashMap;
use std::io::BufRead;
use std::{fs, io};
use walkdir::WalkDir;

#[derive(serde::Deserialize)]
struct CrateBuilder<'i> {
    name: Cow<'i, str>,
    vers: semver::Version,
    deps: Vec<DependencyBuilder<'i>>,
    #[serde(rename = "cksum")]
    _cksum: Cow<'i, str>,
    features: HashMap<Cow<'i, str>, Vec<Cow<'i, str>>>,
    yanked: bool,
    // links: Option<?>,
}

impl CrateBuilder<'_> {
    fn build(self, interner: &mut lasso::Rodeo) -> Crate {
        Crate {
            name: interner.get_or_intern(self.name),
            vers: self.vers,
            deps: self.deps.into_iter().map(|d| d.build(interner)).collect(),
            features: self
                .features
                .iter()
                .map(|(k, v)| {
                    (
                        interner.get_or_intern(k),
                        v.iter().map(|f| interner.get_or_intern(f)).collect(),
                    )
                })
                .collect(),
            yanked: self.yanked,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Crate {
    name: Spur,
    vers: semver::Version,
    deps: Vec<Dependency>,
    features: HashMap<Spur, Vec<Spur>>,
    yanked: bool,
}

#[derive(serde::Deserialize)]
struct DependencyBuilder<'i> {
    name: Cow<'i, str>,
    req: semver::VersionReq,
    features: Vec<Cow<'i, str>>,
    optional: bool,
    default_features: bool,
    target: Option<Cow<'i, str>>,
    // kind: DependencyKind,
}

impl DependencyBuilder<'_> {
    fn build(self, interner: &mut lasso::Rodeo) -> Dependency {
        Dependency {
            name: interner.get_or_intern(self.name),
            req: self.req,
            features: self
                .features
                .iter()
                .map(|f| interner.get_or_intern(f))
                .collect(),
            optional: self.optional,
            default_features: self.default_features,
            target: self.target.map(|t| interner.get_or_intern(t)),
            // kind: self.kind,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Dependency {
    name: Spur,
    req: semver::VersionReq,
    features: Vec<Spur>,
    optional: bool,
    default_features: bool,
    target: Option<Spur>,
    // kind: DependencyKind,
}

#[derive(serde::Deserialize, Debug, Clone, Copy)]
#[serde(rename_all = "lowercase")]
enum DependencyKind {
    Normal,
    Dev,
    Build,
}

fn load_from_index(interner: &mut lasso::Rodeo) -> anyhow::Result<Vec<Crate>> {
    let mut crates = Vec::new();

    let walker = WalkDir::new("crates.io-index")
        .min_depth(1)
        .into_iter()
        .filter_entry(|e| e.file_name() != ".git");

    for entry in walker {
        let entry = entry.context("Unable to get directory entry")?;

        if entry.file_type().is_file() {
            let file = fs::File::open(entry.path()).context("Unable to open index file")?;
            let reader = io::BufReader::new(file);

            for (line_number, line) in reader.lines().enumerate() {
                let line = line.context("Unable to read line")?;

                let crate_builder: CrateBuilder = {
                    let res = serde_json::from_str(&line).with_context(|| {
                        format!(
                            "Unable to parse crate {}, line {}",
                            entry.file_name().to_string_lossy(),
                            line_number + 1
                        )
                    });
                    match res {
                        Ok(crate_builder) => crate_builder,
                        Err(_) => {
                            continue;
                        }
                    }
                };

                let my_crate = crate_builder.build(interner);

                // if my_crate.name == interner.get_or_intern_static("lasso") {
                //     println!("{:?}", my_crate);
                // }

                // dbg!(interner.resolve(&my_crate.name));
                // dbg!(my_crate);

                crates.push(my_crate);
            }
        }
    }

    Ok(crates)
}

fn load_from_file() -> anyhow::Result<(lasso::RodeoResolver, Vec<Crate>)> {
    let resolver_bytes = fs::read("resolver.bc")?;
    let resolver = bincode::deserialize(&resolver_bytes)?;

    let crates_bytes = fs::read("crates.bc")?;
    let crates = bincode::deserialize(&crates_bytes)?;

    Ok((resolver, crates))
}

fn main() -> anyhow::Result<()> {
    let mut interner = lasso::Rodeo::<Spur>::new();
    let crates = load_from_index(&mut interner)?;
    let resolver = interner.into_resolver();

    // Uncomment the following line to load the database from resolver.bc & crates.bc file instead of from the index
    // let (resolver, crates) = load_from_file()?;

    println!("Parsed crates index");

    let resolver_bytes = bincode::serialize(&resolver)?;
    fs::write("resolver.bc", &resolver_bytes)?;

    let crates_bytes = bincode::serialize(&crates)?;
    fs::write("crates.bc", &crates_bytes)?;

    Ok(())
}
